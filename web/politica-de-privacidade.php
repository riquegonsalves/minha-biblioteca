<?php include 'includes/head.php' ?>
  <header class="account">
    <div class="container">
        <a href="./" class="logo">
          <img src="img/logo-branca.png" alt="Sua Biblioteca">
        </a>
        <nav>
          <ul>
            <li>
              <a href="#como-funciona-anchor">Sobre</a>
            </li>
            <li>
              <a href="#solucoes-azul">Soluções</a>
            </li>
            <li>
              <a href="#pacotes-anchor">Pacotes</a>
            </li>
            <li>
              <a href="#servicos-anchor">Serviços</a>
            </li>
            <li>
              <a href="#contato-anchor">Contato</a>
            </li>
						<li class="hover-login">
							<a href="#">Login</a>
		          <div class="div-login">
		            <form class="form-login" action="./dashboard.php">
		              <label for="emailfield">email:</label>
		              <input type="text" name="emailfield">
		              <label for="passwordfield">senha:</label>
		              <input type="password" name="passwordfield">
		              <a class="esqueci-senha" href="#">esqueci minha senha</a>
		              <input type="submit" value="Entrar">
            </li>
          </ul>
        </nav>
      </div>
      <div class="divisao-azul"></div>
  </header>
  <section id="politicas" class="nuvens-bg">
    <div class="container">
      <h1>Termos de Uso</h1>
      <div class="termos">
        <div class="texto">
          <p>
             Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras id eros pulvinar, pharetra sapien non, feugiat orci. Pellentesque auctor, nisl ut dignissim imperdiet, ligula dolor volutpat nisl, eu porta justo leo sit amet elit. Pellentesque pharetra justo lectus, in ornare est dignissim eu. Nulla sodales tortor quis felis finibus, at fringilla leo aliquet. In ultricies enim ac nulla finibus pellentesque. Mauris ac ligula quam. Donec nec tempor est, sagittis pellentesque dui. Donec finibus lacus ut fermentum lacinia. Suspendisse potenti. Etiam tempor massa quis erat blandit cursus nec vitae tellus. Quisque non dolor quis ligula aliquet molestie. Cras pellentesque aliquet metus sit amet fringilla. Mauris elementum justo turpis, eget commodo risus ultricies vel. Mauris porta, elit in accumsan efficitur, mauris nisl cursus mauris, vestibulum vulputate purus arcu ut lacus. Interdum et malesuada fames ac ante ipsum primis in faucibus.
          </p>
          <p>
            Sed at pretium metus, vitae vulputate purus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean id bibendum nisl. Donec facilisis libero eget dapibus auctor. Nullam venenatis elit non erat sagittis, ac pretium enim fringilla. In ornare, nisi dignissim scelerisque tincidunt, dolor turpis vulputate quam, non convallis tortor libero non tellus. Ut eu nunc quis tortor rhoncus tincidunt ac eget massa. Aliquam erat volutpat. Fusce euismod suscipit accumsan. Mauris eu metus hendrerit, ullamcorper nibh eu, tristique diam. Integer nec neque ut mi convallis efficitur.
          </p>
          <p>
            Nam congue pulvinar blandit. Donec ornare tortor dictum elit varius rutrum. Curabitur leo leo, egestas dictum malesuada in, accumsan et leo. Duis eu vulputate massa. Phasellus dignissim molestie odio et vehicula. Donec cursus vulputate ipsum, at interdum diam rhoncus quis. Nunc vitae tincidunt erat. Vivamus vel congue dolor. Ut molestie tortor quis tortor blandit pulvinar a eu nisi. 
          </p>
        </div>
      </div>

      <div class="divisao-cinza"></div>

      <h1>Política de Privacidade</h1>
      <div class="termos">
        <div class="texto">
          <p>
             Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras id eros pulvinar, pharetra sapien non, feugiat orci. Pellentesque auctor, nisl ut dignissim imperdiet, ligula dolor volutpat nisl, eu porta justo leo sit amet elit. Pellentesque pharetra justo lectus, in ornare est dignissim eu. Nulla sodales tortor quis felis finibus, at fringilla leo aliquet. In ultricies enim ac nulla finibus pellentesque. Mauris ac ligula quam. Donec nec tempor est, sagittis pellentesque dui. Donec finibus lacus ut fermentum lacinia. Suspendisse potenti. Etiam tempor massa quis erat blandit cursus nec vitae tellus. Quisque non dolor quis ligula aliquet molestie. Cras pellentesque aliquet metus sit amet fringilla. Mauris elementum justo turpis, eget commodo risus ultricies vel. Mauris porta, elit in accumsan efficitur, mauris nisl cursus mauris, vestibulum vulputate purus arcu ut lacus. Interdum et malesuada fames ac ante ipsum primis in faucibus.
          </p>
          <p>
            Sed at pretium metus, vitae vulputate purus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean id bibendum nisl. Donec facilisis libero eget dapibus auctor. Nullam venenatis elit non erat sagittis, ac pretium enim fringilla. In ornare, nisi dignissim scelerisque tincidunt, dolor turpis vulputate quam, non convallis tortor libero non tellus. Ut eu nunc quis tortor rhoncus tincidunt ac eget massa. Aliquam erat volutpat. Fusce euismod suscipit accumsan. Mauris eu metus hendrerit, ullamcorper nibh eu, tristique diam. Integer nec neque ut mi convallis efficitur.
          </p>
          <p>
            Nam congue pulvinar blandit. Donec ornare tortor dictum elit varius rutrum. Curabitur leo leo, egestas dictum malesuada in, accumsan et leo. Duis eu vulputate massa. Phasellus dignissim molestie odio et vehicula. Donec cursus vulputate ipsum, at interdum diam rhoncus quis. Nunc vitae tincidunt erat. Vivamus vel congue dolor. Ut molestie tortor quis tortor blandit pulvinar a eu nisi. 
          </p>
        </div>
      </div>
    </div>
  </section>
<?php include 'includes/footer.php' ?>