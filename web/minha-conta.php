<?php include 'includes/head.php' ?>
	<header class="account">
		<div class="container">
				<a href="./" class="logo">
					<img src="img/logo-branca.png" alt="Sua Biblioteca">
				</a>
				<nav>
					<div class="bemvindo">
						<span>Olá,</span> Maria do Socorro
					</div>
					<ul>
						<li>
							<a href="./dashboard.php">Dashboard</a>
						</li>
						<li>
							<a href="#" class="active">Minha Conta</a>
						</li>
						<li>
							<a href="./suporte.php">Suporte</a>
						</li>
						<li>
							
							<a href="./">Sair</a>
						</li>
					</ul>
				</nav>
			</div>
			<div class="divisao-azul"></div>
	</header>
	<section id="minha-conta" class="nuvens-bg">
		<div class="container">
			<div class="h-left">
				<div class="box">
					<div class="title">
						<h2>Informações Pessoais</h2>
						<a href="#" id="btn-editar-info">Editar</a>
					</div>
					<div class="info">
						<ul>
							<li>
								<span>Nome:</span> Fulana da Silva Castro
							
							</li>
							<li>
								<span>Telefone:</span> (44) 4004-2002
							
							</li>
							<li>
								<span>E-mail:</span> fulana@test.com
							
							</li>
							<li>
								<span>Endereço:</span> Rua Fulana, No 203, Av. Paulista - Jardins

							</li>
							<li>
								<span>Senha:</span> ***

							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="h-right">
				
				<div class="box">
					<div class="title">
						<h2>Informações de Pagamento</h2>
					</div>
					<div class="info">
						<ul>
							<li>
								<span>Seu Pacote:</span> Pacote Atual
								<a id="alterar-pacote" href="#"> Alterar pacote </a>
							
							</li>
							<li>
								<span>Vencimento:</span> 21/12/2016
							
							</li>
						
						</ul>
					</div>

					<div class="title2">
						<h2>Gerar Boleto de Pagamento</h2>
					</div>
					<div class="info">
						<div class="h-left">
							<span>Do mês:</span>
							<select name="" id="">
								<option value="">Outubro / 2015</option>
							</select>

							<span class="desconto">de R$250,00</span>
							<div class="price">
								<span>por </span>R$ 199,00
							</div>
						</div>
						<div class="h-right">
							<span>Ao mês:</span>
							<select name="" id="">
								<option value="">Outubro / 2015</option>
							</select>
							<button class="btn-gerar-boleto">
								Gerar boleto
							</button>
						</div>
					</div>
					
					<div class="title2">
						<h2>Habilitar Cartão de Crédito</h2>
					</div>
					<div class="info">
						<div class="h-left">
							<span><label for="nometitular">nome do titular:</label></span>
							<input type="text" name="nometitular">
						
							<div id="div-codigoseguranca">
								<span><label for="codigoseguranca">cód. seg.:</label></span>
								<input type="text" name="codigoseguranca" id="campo-codseg-dashboard">
							</div>
							
							<div id="div-validade">
								<span><label for="validade">validade:</label></span>
								<input type="text" id="validade-cartao-mes" name="validademes">
								<input type="text" id="validade-cartao-ano" name="validadeano">
							</div>
						</div>
						<div class="h-right">
							<span><label for="numerocartao">número do Cartão:</label></span>
							<input type="text" name="numerocartao">
							<button>
								Habilitar
							</button>
						</div>
					</div>
					
					<div class="title2">
						<h2>Seus Dados de Pagamento</h2>
					</div>
					<div class="info">
						<div class="h-left">
							<span><label for="nometitular">nome do titular:</label></span>
							<input type="text" name="nometitular" placeholder="Maria do Socorro Bizantino Freitas">
						
							<div id="div-codigoseguranca">
								<span><label for="codigoseguranca">cód. seg.:</label></span>
								<input type="text" name="codigoseguranca" id="campo-codseg-dashboard" placeholder="127">
							</div>
							
							<div id="div-validade">
								<span><label for="validade">validade:</label></span>
								<input type="text" id="validade-cartao-mes" name="validademes" placeholder="10">
								<input type="text" id="validade-cartao-ano" name="validadeano" placeholder="2021">
							</div>
						</div>
						<div class="h-right">
							<span><label for="numerocartao">número do Cartão:</label></span>
							<input type="text" name="numerocartao" placeholder="*****8907">
							<button>
								Atualizar
							</button>
						</div>
						<div class="clear">
						</div>
						<div class="div-voltar-boleto">
							<a class="voltar-boleto">> Quero voltar a pagar via boleto</a>
						</div>
					</div>
				</div>
				
				

				<div class="box">
					<div class="title">
						<h2>Notas Fiscais</h2>
					</div>
					<div class="info info-nf">
						<a class="wrap" href="#">
							<h4>NFe referente ao período de:</h4>
							<h5 class="data">Setembro/2015</h5>
						</a>
					</div>
					<div class="info info-nf">
						<a class="wrap" href="#">
							<h4>NFe referente ao período de:</h4>
							<h5 class="data">Setembro/2015</h5>
						</a>
					</div>
					<div class="info info-nf">
						<a class="wrap" href="#">
							<h4>NFe referente ao período de:</h4>
							<h5 class="data">Setembro/2015</h5>
						</a>
					</div>
				</div>

			</div>
		</div>
	</section>
<?php include 'includes/footer.php' ?>