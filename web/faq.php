<?php include 'includes/head.php' ?>
<header class="account">
	<div class="container">
		<a href="./" class="logo">
			<img src="img/logo-branca.png" alt="Sua Biblioteca">
		</a>
		<nav>
			<ul>
				<li>
					<a href="#como-funciona-anchor">Sobre</a>
				</li>
				<li>
					<a href="#solucoes-azul">Soluções</a>
				</li>
				<li>
					<a href="#pacotes-anchor">Pacotes</a>
				</li>
				<li>
					<a href="#servicos-anchor">Serviços</a>
				</li>
				<li>
					<a href="#contato-anchor">Contato</a>
				</li>
				<li class="hover-login">
					<a href="#">Login</a>
          <div class="div-login">
            <form class="form-login" action="./dashboard.php">
              <label for="emailfield">email:</label>
              <input type="text" name="emailfield">
              <label for="passwordfield">senha:</label>
              <input type="password" name="passwordfield">
              <a class="esqueci-senha" href="#">esqueci minha senha</a>
              <input type="submit" value="Entrar">
            </form>
          </div>
				</li>
			</ul>
		</nav>
	</div>
	<div class="divisao-azul"></div>
</header>
<section class="nuvens-bg" id="faq">
	<div class="container">
		<h1>Perguntas frequentes <div class="int1">?</div><div class="int2">?</div><div class="int3">?</div></h1>

		<ul class="faq-options">
			<li>
				<p>
					<span>1.</span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere aperiam eligendi molestiae dolore?
				</p>
				<div class="answer">
					<p>
						<span>Resposta:</span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae ipsa modi neque, architecto beatae molestiae veniam magni aut id commodi? Cumque consequuntur officia, accusamus cupiditate necessitatibus perferendis omnis expedita officiis!
					</p>
				</div>
			</li>
			<li>
				<p>
					<span>2.</span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere aperiam eligendi molestiae dolore?
				</p>
				<div class="answer">
					<p>
						<span>Resposta:</span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae ipsa modi neque, architecto beatae molestiae veniam magni aut id commodi? Cumque consequuntur officia, accusamus cupiditate necessitatibus perferendis omnis expedita officiis!
					</p>
				</div>
			</li>
			<li>
				<p>
					<span>3.</span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere aperiam eligendi molestiae dolore?
				</p>
				<div class="answer">
					<p>
						<span>Resposta:</span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae ipsa modi neque, architecto beatae molestiae veniam magni aut id commodi? Cumque consequuntur officia, accusamus cupiditate necessitatibus perferendis omnis expedita officiis!
					</p>
				</div>
			</li>
			<li>
				<p>
					<span>4.</span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere aperiam eligendi molestiae dolore?
				</p>
				<div class="answer">
					<p>
						<span>Resposta:</span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae ipsa modi neque, architecto beatae molestiae veniam magni aut id commodi? Cumque consequuntur officia, accusamus cupiditate necessitatibus perferendis omnis expedita officiis!
					</p>
				</div>
			</li>
			<li>
				<p>
					<span>5.</span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere aperiam eligendi molestiae dolore?
				</p>
				<div class="answer">
					<p>
						<span>Resposta:</span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae ipsa modi neque, architecto beatae molestiae veniam magni aut id commodi? Cumque consequuntur officia, accusamus cupiditate necessitatibus perferendis omnis expedita officiis!
					</p>
				</div>
			</li>
		</ul>
	</div>
</section>
<?php include 'includes/footer.php' ?>
