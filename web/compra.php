<?php include "includes/head.php" ?>
<header class="account">
	<div class="container">
		<a href="./" class="logo">
			<img src="img/logo-branca.png" alt="Sua Biblioteca">
		</a>
		<nav>
			<ul>
				<li>
					<a href="/#como-funciona-anchor">Sobre</a>
				</li>
				<li>
					<a href="/#solucoes-azul">Soluções</a>
				</li>
				<li>
					<a href="/#pacotes-anchor">Pacotes</a>
				</li>
				<li>
					<a href="/#servicos-anchor">Serviços</a>
				</li>	
				<li>
					<a href="/#contato-anchor">Contato</a>
				</li>
				<li class="hover-login">
					<a href="#">Login</a>
					<div class="div-login">
						<form class="form-login" action="./dashboard.php">
							<label for="emailfield">email:</label>
							<input type="text" name="emailfield">
							<label for="passwordfield">senha:</label>
							<input type="password" name="passwordfield">
							<a class="esqueci-senha" href="#">esqueci minha senha</a>
							<input type="submit" value="Entrar">
						</form>
					</div>
				</li>
			</ul>
		</nav>
	</div>
	<div class="divisao-azul"></div>
</header>
<section class="nuvens-bg" id="compra">
	<div class="container">
		<div class="passo-passo">
			<div class="passo active">
				<p>ESCOLHA O PACOTE</p>
				<div class="num"> 1 </div>
			</div>
			<div class="passo active">
				<p>ESCOLHA OS SERVIÇOS</p>
				<div class="num"> 2 </div>
			</div>
			<div class="passo active">
				<p>FAÇA O PAGAMENTO</p>
				<div class="num"> 3 </div>
			</div>
		</div>

		<h1>Finalização da Compra</h1>


		<form action="#">
			<div class="box">
				<div class="title">
					<h2>Informações Pessoais</h2>
				</div>
				
				<div class="content">
					
					<div class="esq">
						<label for="name">nome*</label>
						<input type="text" name="name">
						<label for="phone">telefone*</label>
						<input type="text" name="phone">
					</div>
					<div class="dir">
						<label for="email">email*</label>
						<input type="email" name="email">
					</div>
				</div>
				<div class="endereco">
					<div class="cep">
						<label for="cep">cep*</label>
						<input type="text" name="cep">
					</div>
					<div class="address">
						<label for="address">endereço*</label>
						<input type="text" name="address">
					</div>
					<div class="number">
						<label for="number">número*</label>
						<input type="text" name="number">
					</div>
				</div>
			</div>

			<div class="box">
				<div class="title">
					<h2>Informações da Empresa <span>(opcional)</span> </h2>
				</div>

				<div class="content">
					<div class="esq">
						<label for="nameCompany">nome fantasia</label>
						<input type="text" name="nameCompany">
						<label for="addressCompany">endereço</label>
						<input type="text" name="addressCompany">
					</div>
					<div class="dir">
						<label for="phoneCompany">telefone</label>
						<input type="text" name="phoneCompany">
						<label for="cnpj">cnpj</label>
						<input type="text" name="cpnj">
					</div>

				</div>
				
				<div class="url">
					<div class="field">
						<label for="url">escolha a url da sua biblioteca</label>
						<input type="text" name="url">
					</div>
					<div class="bb">
						.suabiblioteca.com.br
					</div>
					<div class="disponibilidade">
						<button>
							Verificar disponibilidade
						</button>
					</div>
				</div>

				<div class="content">
					
					<div class="esq"><label for="password">senha</label>
						<input type="password" name="password">
					</div>
					<div class="dir">
						<label for="password-confirm">confirmar senha</label>
						<input type="password" name="password-confirm">
					</div>
				</div>
				<div class="termos">
					<input type="checkbox" name="terms" id="terms">
					<label for="terms">Li e aceito os <a href="#" target="_blank">TERMOS DE USO</a></label>
				</div>
			</div>

			<div class="tabs box">
				<div class="title">
					<h2>Informações de Pagamento</h2>

				</div>
				
				<ul class="op-payment">
					<li class="tab-boleto selecionado">Boleto</li>
					<li class="tab-cartao">Cartão de Crédito</li>
				</ul>
				
				<div class="content">
					<div class="compra-geral compra-boleto">
						<p>
							Em minutos, você receberá, em seu email, o boleto bancário para o pagamento do pacote de soluções que escolheu.
		Para acompanhar o status do seu pagamento e a liberação da sua biblioteca, faça login no nosso site utilizando seu
		email e a senha informada nos campos acima.
						</p>
					</div>
		
					<div class="compra-geral compra-cartao">
						<div class="bandeiras">
						
							<label id="title-escolha-bandeira">Escolha a bandeira:</label>
						
							<input type="radio" id="bandeiravisa" name="bandeira" value="visa">
							<label for="bandeiravisa">Visa</label>
						
							<input type="radio" id="bandeiramaster" name="bandeira" value="master">
							<label for="bandeiramaster">Master</label>
						
						</div>
						<div class="content">
							<div class="esq">
								<label for="nometitular">nome do titular</label>
								<input type="text" name="nometitular">
							</div>
							<div class="dir">
								<label for="numerocartao">Número do Cartão</label>
								<input type="text" name="numerocartao">
							</div>
						</div>
						<div class="complemento1">
						<label for="codigoseguranca">Código de Segurança</label>
						<input type="text" name="codigoseguranca">
						</div>
						<div class="complemento2">
						<label for="validade">data de validade</label>
						<input type="text" id="validade-cartao-mes" name="validademes" placeholder="mm">
						<input type="text" id="validade-cartao-ano" name="validadeano" placeholder="aaaa">
						</div>
					</div>
				</div>
				
			</div>

			<button class="finalizar" type="submit">Finalizar Compra</button>
		</form>
	</div>
</section>
<?php include "includes/footer.php" ?>