var position = 0;
$(document).ready(function(){

  $('.mobilehead .button').click(function(){
  	$('header').toggleClass('aberto');
  });


  $('button.new').click(function(){
  	$('#novo-chamado').css('left', 0);
    position = $(window).scrollTop();
    $('.close').show();
  });


  var scrollFunction = function(){

  	var diff;

  	var scrolltop = $(this).scrollTop();

  	var index = $(this).scrollTop()/2;
  	var escuro = index/3;
  	$('.n-azul-1').css('top', 200 - index + 'px');
  	$('.n-azul-3').css('top', 30 - index + 'px');
  	$('.n-azul-2').css('bottom', 180 + index + 'px');
  	$('.n-azul-4').css('bottom', 160 + index + 'px');
  
  	$('#como-funciona .nuvens-escuras div').css({
  		'top': 0 - escuro + 'px'
  	});

    console.log(scrolltop);

    if(scrolltop < 300){
      $('div#btn-top').removeClass('cd-is-visible');
      $('div#btn-top').removeClass('cd-fade-out');
      $('div#btn-top').addClass('cd-top').fadeOut(500);
    
    }

  	if(scrolltop > 300){
      $('div#btn-top').addClass('cd-is-visible'); 
      $('div#btn-top').removeClass('cd-top').fadeIn(500); 
    }


  	if(scrolltop > 690){

  		dif = 930 - scrolltop;


  		if( dif > 0){
  			$('.icon img.livro').css({
  			  '-webkit-transform' : 'scale(' + Math.abs((dif-240)/240) + ')',
  			  '-moz-transform'    : 'scale(' + Math.abs((dif-240)/240) + ')',
  			  '-ms-transform'     : 'scale(' + Math.abs((dif-240)/240) + ')',
  			  '-o-transform'      : 'scale(' + Math.abs((dif-240)/240) + ')',
  			  'transform'         : 'scale(' + Math.abs((dif-240)/240) + ')',
  			  'opacity' :  Math.abs((dif-240)/240)
  			});

  			$('.funciona-1 .texto').css({
  			  'opacity' :  Math.abs((dif-240)/240)
  			});
  		} else {
  			$('.icon img.livro').css({
  			  '-webkit-transform' : 'scale(' + 1 + ')',
  			  '-moz-transform'    : 'scale(' + 1 + ')',
  			  '-ms-transform'     : 'scale(' + 1 + ')',
  			  '-o-transform'      : 'scale(' + 1 + ')',
  			  'transform'         : 'scale(' + 1 + ')',
  			  'opacity' :  1
  			});

  			$('.funciona-1 .texto').css({
  			  'opacity' :  1
  			});
  		}

  	} 

  	if(scrolltop > 985){

  		dif = 1280 - $(this).scrollTop();
  		if(dif > 0){
  			$('.icon img.caixa').css({
  			  '-webkit-transform' : 'scale(' + Math.abs((dif-295)/295) + ')',
  			  '-moz-transform'    : 'scale(' + Math.abs((dif-295)/295) + ')',
  			  '-ms-transform'     : 'scale(' + Math.abs((dif-295)/295) + ')',
  			  '-o-transform'      : 'scale(' + Math.abs((dif-295)/295) + ')',
  			  'transform'         : 'scale(' + Math.abs((dif-295)/295) + ')',
  			  'opacity' :  Math.abs((dif-295)/295)
  			});

  			$('.funciona-2 .texto').css({
  			  'opacity' :  Math.abs((dif-295)/295)
  			});
  		} else {
  			$('.icon img.caixa').css({
  			  '-webkit-transform' : 'scale(' + 1 + ')',
  			  '-moz-transform'    : 'scale(' + 1 + ')',
  			  '-ms-transform'     : 'scale(' + 1 + ')',
  			  '-o-transform'      : 'scale(' + 1 + ')',
  			  'transform'         : 'scale(' + 1 + ')',
  			  'opacity' :  1
  			});

  			$('.funciona-2 .texto').css({
  			  'opacity' :  1
  			});
  		}
  	}

  	if(scrolltop > 1320){

  		dif = 1680 - $(this).scrollTop();
  		if(dif > 0){
  			$('.icon img.icon-nuvem').css({
  			  '-webkit-transform' : 'scale(' + Math.abs((dif-360)/360) + ')',
  			  '-moz-transform'    : 'scale(' + Math.abs((dif-360)/360) + ')',
  			  '-ms-transform'     : 'scale(' + Math.abs((dif-360)/360) + ')',
  			  '-o-transform'      : 'scale(' + Math.abs((dif-360)/360) + ')',
  			  'transform'         : 'scale(' + Math.abs((dif-360)/360) + ')',
  			  'opacity' :  Math.abs((dif-360)/360)
  			});

  			$('.funciona-3 .texto').css({
  			  'opacity' :  Math.abs((dif-360)/360)
  			});
  		} else {
  			$('.icon img.icon-nuvem').css({
  			  '-webkit-transform' : 'scale(' + 1 + ')',
  			  '-moz-transform'    : 'scale(' + 1 + ')',
  			  '-ms-transform'     : 'scale(' + 1 + ')',
  			  '-o-transform'      : 'scale(' + 1 + ')',
  			  'transform'         : 'scale(' + 1 + ')',
  			  'opacity' :  1
  			});

  			$('.funciona-3 .texto').css({
  			  'opacity' :  1
  			});
  		}
  	}
  
  
  
  	if(scrolltop > 2395 && scrolltop < 3125){

  		var total = 3125 - 2395; // 730
  		var pos =  3125 - $(this).scrollTop();
        
  		if(pos*85/total < 50 && pos*85/total >= 20){
  			$('.indexacao').css({
  			  '-webkit-transform' : 'scale(' + 1 + ')',
  			  '-moz-transform'    : 'scale(' + 1 + ')',
  			  '-ms-transform'     : 'scale(' + 1 + ')',
  			  '-o-transform'      : 'scale(' + 1 + ')',
  			  'transform'         : 'scale(' + 1 + ')',
  			  'opacity' :  1
  			});

      

  			if(!$('.text.indexacao').is(':visible')){	
  				$('.fade').fadeOut(300, function(){
  					$('.text.gestao, .text.biblioteca').hide();
  					$('.text.indexacao').show();
  					$('.fade').fadeIn(300);
            $('#proxima-solucao1').fadeOut(100, function(){
              $('#solucao-pratica').fadeOut(100);
              $('#proxima-solucao2').fadeIn(100);
            });
  				});
  			}
      

  			// $('.text.gestao, .text.biblioteca').fadeOut(300, function(){
		
  			// 	$('.text.indexacao').delay(300).fadeIn(300);
  			// });

  		} else if(pos*85/total < 20){
  			$('.biblioteca').css({
  			  '-webkit-transform' : 'scale(' + 1 + ')',
  			  '-moz-transform'    : 'scale(' + 1 + ')',
  			  '-ms-transform'     : 'scale(' + 1 + ')',
  			  '-o-transform'      : 'scale(' + 1 + ')',
  			  'transform'         : 'scale(' + 1 + ')',
  			  'opacity' :  1
  			});
  			// $('.text.indexacao, .text.gestao').fadeOut(300, function(){
  			// 	$('.text.biblioteca').delay(300).fadeIn(300);
  			// });
			
  			if(!$('.text.biblioteca').is(':visible')){	

  				$('.fade').fadeOut(300, function(){
  					$('.text.indexacao, .text.gestao').hide();
  					$('.text.biblioteca').show();
  					$('.fade').fadeIn(300);
            $('#proxima-solucao2').fadeOut(100, function(){
              $('#solucao-pratica').fadeIn(100);
            });
  				});

  			}


  		} else {


  			if(!$('.text.gestao').is(':visible')){
  				$('.fade').fadeOut(300, function(){
  					$('.text.indexacao, .text.biblioteca').hide();
  					$('.text.gestao').show();
  					$('.fade').fadeIn(300);
            $('#proxima-solucao2').fadeOut(100, function(){
              $('#proxima-solucao1').fadeIn(100);
            });
  				});
  			}

  			// $('.text.indexacao, .text.biblioteca').fadeOut(300, function(){
  			// 	$('.text.gestao').delay(300).fadeIn(300);
  			// });
			
  		}


  		$('.solucoes ul .nuvem').css('right', 
  		pos*85/total+'%');

  		if($('.solucoes-mobile').css('display') == 'none'){
  			$('.solucoes').fadeIn(300);
  		}

  		var solucoesWidth = parseInt($('.solucoes').css('width'));

  		$('.solucoes').css({
  			'position': 'fixed',
  			'top': '150px',
  			'left': '50%',
  			'margin-left': '-'+solucoesWidth/2+'px',
  			'max-width': '1060px'
  		});
  	} else if(scrolltop > 2925){
  		$('.solucoes').fadeOut(300);
  	} else {
  		$('.solucoes').css({
  			'position': 'relative',
  			'top': 'auto',
  			'left': 'auto',
  			'margin-left': 'auto'
  		});
  	}


  	if( scrolltop > 5010 ){

  		$('#pacotes .nuvens-escuras div').css({
  			'top': 0 - (scrolltop - 5010) / 6 + 'px'
  		});
  	}


  };

  $(document).scroll(scrollFunction);


  $(document).ready(function(){
  	if($(window).scrollTop() > 490){
  		scrollFunction();
  	}

  	if($('.nuvens-bg').length){
  		$(document).scroll(function(){
  			$('.nuvens-bg').css('background-position-y', $(window).scrollTop()/3+'px');
  			$('.nuvens-bg').css('background-position-x', 'center');
  		});
  	}
  });

  $('.btn-pedido-orcamento').click(function(){
  	$('#formulario').css({
  		'left': 0
  	});
    $('body').css({
      'overflow': 'hidden'
    });
    position = $(window).scrollTop();
    $('.close').show();
  });

  $('.btn-req-demo').click(function(){
  	$('#formulario-req-demo').css({
  		'left': 0
  	});
    $('body').css({
      'overflow': 'hidden'
    });
    position = $(window).scrollTop();
    $('.close').show();
  });

  $('.btn-req-personal').click(function(){
  	$('#formulario-req-personal').css({
  		'left': 0
  	});
    $('body').css({
      'overflow': 'hidden'
    });

    position = $(window).scrollTop();
    $('.close').show();
  });

  $('.btn-gerar-boleto').click(function(){
  	$('#boleto-nf').css({
  		'left': 0
  	});
    $('body').css({
      'overflow': 'hidden'
    });

    position = $(window).scrollTop();
    $('.close').show();
  });

  // $('#formulario .close').click(function(){
  // 	$('#formulario').css({
  // 		'left': '-100vw'
  // 	});
  //   $('body').css({
  //     'overflow-x': 'hidden',
  //     'overflow-y': 'auto',
  //     'height': 'auto'
  //   });
  // });

  // $('#formulario-req-demo .close').click(function(){
  // 	$('#formulario-req-demo').css({
  // 		'left': '-100vw'
  // 	});
  //   $('body').css({
  //     'overflow-x': 'hidden',
  //     'overflow-y': 'auto',
  //     'height': 'auto'
  //   });
  // });

  // $('#formulario-req-personal .close').click(function(){

  // 	$('#formulario-req-personal').css({
  // 		'left': '-100vw'
  // 	});

  //   $('html, body').animate({
  //     scrollTop: position
  //   }, 500);

  // });

  // $('#formulario-esqueceu-senha .close').click(function(){
  // 	$('#formulario-esqueceu-senha').css({
  // 		'left': '-100vw'
  // 	});
  //   $('body').css({
  //     'overflow-x': 'hidden',
  //     'overflow-y': 'auto',
  //     'height': 'auto'
  //   });

  //    $('html, body').animate({
  //     scrollTop: position
  //   }, 500);
  // });

  // $('#formulario-editar-dados .close').click(function(){
  // 	$('#formulario-editar-dados').css({
  // 		'left': '-100vw'
  // 	});
  //   $('body').css({
  //     'overflow-x': 'hidden',
  //     'overflow-y': 'auto',
  //     'height': 'auto'
  //   });

  //    $('html, body').animate({
  //     scrollTop: position
  //   }, 500);
  // });

  // $('#formulario-alterar-pacote .close').click(function(){
  // 	$('#formulario-alterar-pacote').css({
  // 		'left': '-100vw'
  // 	});
  //   $('body').css({
  //     'overflow-x': 'hidden',
  //     'overflow-y': 'auto',
  //     'height': 'auto'
  //   });

  //    $('html, body').animate({
  //     scrollTop: position
  //   }, 500);
  // });

  // $('#novo-chamado .close').click(function(){
  // 	$('#novo-chamado').css({
  // 		'left': '-100vw'
  // 	});
  //   $('body').css({
  //     'overflow-x': 'hidden',
  //     'overflow-y': 'auto',
  //     'height': 'auto'
  //   });

  //    $('html, body').animate({
  //     scrollTop: position
  //   }, 500);
  // });

  $('.close').click(function(){
  	$('#boleto-nf, #novo-chamado, #formulario-alterar-pacote, #formulario-editar-dados, #formulario-esqueceu-senha, #formulario-req-demo, #formulario-req-personal, #formulario').css({
  		'left': '-100vw'
  	});
    $('.close').hide();
    $('body').css({
      'overflow-x': 'hidden',
      'overflow-y': 'auto',
      'height': 'auto'
    });

     $('html, body').animate({
      scrollTop: position
    }, 500);
  });

  $('.faq-options li').click(function(){
  	$(this).toggleClass('open');
  });



  $(document).keyup(function(e) {
      if (e.keyCode == 27) { 
          $('#formulario-req-personal, #formulario-req-demo, #formulario, #novo-chamado').css({
  			'left': '-100vw'
  		});

          $('.close').hide();
          $('body').css({
            'overflow-x': 'hidden',
            'overflow-y': 'auto',
            'height': 'auto' 
          });

          $('html, body').animate({
            scrollTop: position
          }, 500);
      }
  });

  $(document).keyup(function(e) {
      if (e.keyCode == 27) { 
          $('#formulario-alterar-pacote').css({
  			'left': '-100vw'
  		});
          $('.close').hide();

          $('body').css({
            'overflow-x': 'hidden',
            'overflow-y': 'auto',
            'height': 'auto' 
          });

          $('html, body').animate({
            scrollTop: position
          }, 500);
      }
  });

  $(document).keyup(function(e) {
      if (e.keyCode == 27) { 
          $('#formulario-esqueceu-senha').css({
  			'left': '-100vw'
  		});
          $('.close').hide();

          $('body').css({
            'overflow-x': 'hidden',
            'overflow-y': 'auto',
            'height': 'auto' 
          });

          $('html, body').animate({
            scrollTop: position
          }, 500);
      }
  });

  $(document).keyup(function(e) {
      if (e.keyCode == 27) { 
          $('#boleto-nf').css({
  			'left': '-100vw'
  		});
          $('.close').hide();

          $('body').css({
            'overflow-x': 'hidden',
            'overflow-y': 'auto',
            'height': 'auto' 
          });

          $('html, body').animate({
            scrollTop: position
          }, 500);
      }
  });

  $('.check .orcamento').click(function(){
    $(this).toggleClass('checked');

  });


  var firefox = false;
  if(navigator.userAgent.indexOf('Firefox') !== -1){
  	firefox = true;
  }

  if(!firefox){
  	$('a').click(function(){
      if($(this).attr('id') == 'proxima-solucao1'){
    	  $('html, body').animate({
    	    scrollTop: 2700
    	  }, 2000);
      }else if($(this).attr('id') == 'proxima-solucao2'){
    	  $('html, body').animate({
    	    scrollTop: 2955
    	  }, 2000);
      }else if($(this).attr('id') == 'solucao-pratica'){
    	  $('html, body').animate({
    	    scrollTop: 4005
    	  }, 2000);
      }else if($(this).attr('class') == 'esqueci-senha' || $(this).attr('id') == 'esqueci-senha-mobile'){
      	$('#formulario-esqueceu-senha').css({
      		'left': 0
      	});
      }else if($(this).attr('id') == 'btn-editar-info'){
      	$('#formulario-editar-dados').css({
      		'left': 0
      	});
      }else if($(this).attr('id') == 'alterar-pacote'){
      	$('#formulario-alterar-pacote').css({
      		'left': 0
      	});
      }else if($(this).attr('id') == 'btn-print'){
        var restorepage = document.body.innerHTML;
        var printcontent = document.getElementById("print-document").innerHTML;
        document.body.innerHTML = printcontent;
        window.print();
        document.body.innerHTML = restorepage;
      }else if($(this).attr('class') == 'wrap'){
      	$('#boleto-nf').css({
      		'left': 0
      	});
      }else{
    	  $('html, body').animate({
    	    scrollTop: $( $(this).attr('href') ).offset().top
    	  }, 2000);
      }
  	  return false;
  	});
  
  }

  $("#validade-cartao-mes").mask('00');
  $("#validade-cartao-ano").mask('0000');

  $(".op-payment li").click(function(){
    if($(this).hasClass("tab-boleto")){
      $(".tab-boleto").addClass("selecionado");
      $(".tab-cartao").removeClass("selecionado");
      $(".compra-geral").fadeOut(50, function(){
        $(".compra-boleto").fadeIn(300);
      });
    }else if($(this).hasClass("tab-cartao")){
      $(".tab-cartao").addClass("selecionado");
      $(".tab-boleto").removeClass("selecionado");
      $(".compra-geral").fadeOut(50, function(){
        $(".compra-cartao").fadeIn(300);
      });
    }
  });

  $('#login-mobile').click(function(){
    $(".login-mobile-anchor").focus();
  });


  $(window).load(function () {
     $('#preloader').fadeOut(300);

      // setTimeout(function () {
      //     alert('page is loaded and 1 minute has passed');   
      // }, 60000);

  });
});
