<footer>
	<div class="container">
		<img src="img/logo-footer.png" alt="">
		<ul>
			<li>
				<a href="./politica-de-privacidade.php">Política de Privacidade</a>
			</li>
			<li>
				<a href="./faq.php">Perguntas Frequentes</a>
			</li>
		</ul>
	</div>
	<div class="bottom">
		<p>SuaBiblioteca® Todos os direitos reservados © 2015</p>
	</div>
</footer>
<div id="formulario">
	<div class="container">
		<div class="close"> x </div>
		<div class="nuvem-top-esq"></div>
		<div class="nuvem-top-dir"></div>
		<div class="nuvem-bottom-esq"></div>
		<div class="nuvem-bottom-dir"></div>
		<h1>Requisite o orçamento do serviço TAL</h1>
		<h2>Obrigado pelo interesse! Responderemos a sua requisição nas próximas 24 horas</h2>
		<form action="#">
			<div class="left">
				<label for="name">Nome:</label>
				<input type="text" name="name">
				<label for="email">E-mail:</label>
				<input type="email" name="email">
				<label for="company">Nome da empresa se houver:</label>
				<input type="text" name="company">
			</div>
			<div class="right">
				<label for="phone">Telefone:</label>
				<input type="text" name="phone">
				<label for="cpf">CPF/CNPJ:</label>
				<input type="cpf" name="cpf">
				<label for="about">Como ficou sabendo de nós?</label>
				<input type="text" name="about">
			</div>
			<div class="center">
				<label for="need">Nos explique suas necessidades:</label>
				<textarea name="need" id="need" ></textarea>
			</div>
			<button>Enviar requisição</button>
		</form>
	</div>
</div>
<div id="formulario-req-demo">
	<div class="container">
		<div class="close"> x </div>
		<div class="nuvem-top-esq"></div>
		<div class="nuvem-top-dir"></div>
		<div class="nuvem-bottom-esq"></div>
		<div class="nuvem-bottom-dir"></div>
		<h1>Requisite a demonstração da SuaBiblioteca®</h1>
		<h2>Obrigado pelo interesse! Responderemos a sua requisição nas próximas 24 horas</h2>
		<form action="#">
			<div class="left">
				<label for="name">Nome:</label>
				<input type="text" name="name">
				<label for="email">E-mail:</label>
				<input type="email" name="email">
				<label for="company">Nome da empresa se houver:</label>
				<input type="text" name="company">
			</div>
			<div class="right">
				<label for="phone">Telefone:</label>
				<input type="text" name="phone">
				<label for="cpf">CPF/CNPJ:</label>
				<input type="cpf" name="cpf">
				<label for="about">Como ficou sabendo de nós?</label>
				<input type="text" name="about">
			</div>
			<div class="checks">
				<span>Escolha o tipo da demonstração:</span>
				<input type="checkbox" name="presencial" id="presencial">
				<label for="presencial"><i></i>Apresentação Presencial</label>
				<input type="checkbox" name="internet" id="internet"><label for="internet"><i></i>Link para acesso na Internet</label>
			</div>
			<button>Enviar requisição</button>
		</form>
	</div>
</div>
<div id="formulario-req-personal">
	<div class="container">
		<div class="close"> x </div>
		<div class="nuvem-top-esq"></div>
		<div class="nuvem-top-dir"></div>
		<div class="nuvem-bottom-esq"></div>
		<div class="nuvem-bottom-dir"></div>
		<h1>Requisite o seu pacote personalizado</h1>
		<h2>Obrigado pelo interesse! Responderemos a sua requisição nas próximas 24 horas</h2>
		<form action="#">
			<div class="left">
				<label for="name">Nome:</label>
				<input type="text" name="name">
				<label for="email">E-mail:</label>
				<input type="email" name="email">
				<label for="company">Nome da empresa se houver:</label>
				<input type="text" name="company">
			</div>
			<div class="right">
				<label for="phone">Telefone:</label>
				<input type="text" name="phone">
				<label for="cpf">CPF/CNPJ:</label>
				<input type="cpf" name="cpf">
				<label for="about">Como ficou sabendo de nós?</label>
				<input type="text" name="about">
			</div>
			<div class="center">
				<label for="need">Nos explique suas necessidades:</label>
				<textarea name="need" id="need" ></textarea>
			</div>
			<button>Enviar requisição</button>
		</form>
	</div>
</div>
<div id="formulario-esqueceu-senha">
	<div class="container">
		<div class="close"> x </div>
		<h1>Esqueceu a sua senha?</h1>
		<h2>Nos informe o seu email de cadastro e nós enviaremos as instruções de alteração de senha para a sua caixa de entrada.</h2>
		<form action="#">
			<div>
				<label for="name">Email:</label>
				<input type="text" name="name">
			</div>
			<button>Enviar Instruções</button>
		</form>
	</div>
</div>
<div id="formulario-editar-dados">
	<div class="container">
		<div class="close"> x </div>
    
		<h1>Dados Pessoais</h1>
		<h2>Atualize os seus dados pessoais e confirme as atualizações clicando no botão.</h2>
		<form action="#">
				<div class="info">
					<ul>
						<li>
              <label for="name">Nome:</label>
							<input type="text" name="name" value="Fulana da Silva Castro">			
						</li>
						<li>
              <label for="phone">Telefone:</label>
							<input type="text" name="phone" value="(44) 4004-2002">
						</li>
						<li>
              <label for="email">Email:</label>
							<input type="text" name="email" value="fulana@test.com">
						</li>
						<li>
              <label for="address">Endereço:</label>
							<input type="text" name="address" value="Rua Fulana, No 203, Av. Paulista - Jardins">
						</li>
						<li>
              <label for="new-pw">Nova Senha:</label>
							<input type="password" name="new-pw">
						</li>
						<li>
              <label for="new-pw-confirm">Confirme Nova Senha:</label>
							<input type="password" name="new-pw-confirm">
						</li>
						<li>
              <label for="pw">Sua Senha Atual:</label>
							<input type="password" name="pw">
						</li>
					</ul>
				</div>
			</div>
			<div>
				<button>Atualizar Dados</button>
			</div>
		</form>
	</div>
</div>
<div id="formulario-alterar-pacote">
	<div class="container">
		<div class="close"> x </div>
		<h1>Altere o seu pacote de soluções</h1>
		<h2>Basta escolher o seu novo pacote de soluções e nós realizaremos as alterações no sistema automaticamente. Os ajustes financeiros acontecerão no pagamento do seu próximo ciclo de assinatura.</h2>
		<form action="#">
			<div>
				<ul>
					<li>
							<input type="radio" id="pacote1" name="nome-pacote" value="pacote1"> <label for="pacote1">Nome Pacote (R$99,99/mês)</label>
					</li>
					<li>
						<div>
							<input type="radio" id="pacote2" name="nome-pacote" value="pacote2"> <label for="pacote2">Nome Pacote (R$99,99/mês)</label>
						</div>
					</li>
					<li>
						<div>
							<input type="radio" id="pacote3" name="nome-pacote" value="pacote3"> <label for="pacote3">Nome Pacote (R$99,99/mês)</label>
						</div>
					</li>
					<li>
						<div>
							<input type="radio" id="pacote4" name="nome-pacote" value="pacote4"> <label for="pacote4">Nome Pacote (R$99,99/mês)</label>
						</div>
					</li>
				</ul>
			</div>
			<button>Alterar Pacote</button>
		</form>
	</div>
</div>
<div id="boleto-nf">
	<div class="container">
		<div class="close"> x </div>
		<h1>Boleto/NF <b>Outubro/2016</b></h1>
		<div>
			<div id="print-document">
				<img src="img/boleto-exemplo.png"></img>
			</div>
			<div class="print-icon">
				<a href="#" id="btn-print"><img src="img/print-icon.png"></img>
			</div>
		</div>
	</div>
</div>
<script src="js/jquery.js"></script>
<script src="js/mask.js"></script>
<script src="js/app.js"></script>

<!--testando se o navegador é mais antigo que ie9-->
<script type="text/javascript">
	var ua = window.navigator.userAgent;
	var msie = ua.indexOf("MSIE ");
	
	if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
		alert("Oops! Parece que você está utilizando Internet Explorer. O seu navegador não suporta todos os efeitos visuais presentes neste site e não possui os mecanismos de segurança atualizados. Sugerimos que utilize outros navegadores, como Mozilla Firefox, Opera, Google Chrome, etc. Esta é uma dica para a sua segurança e para uma melhor experiência no nosso sita. =)");
	}
</script>

<!-- <script src="js/app.min.js"></script> -->
</body>
</html>