
	<?php include 'includes/head.php' ?>
		<section id="preloader">
			<div class="container">
				<div class="logo">
					<img src="img/logo-load.png" alt="">
				</div>
				<div class="load">
					<svg width='120px' height='120px' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-default"><rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#8e9191' transform='rotate(0 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#8e9191' transform='rotate(30 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.08333333333333333s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#8e9191' transform='rotate(60 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.16666666666666666s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#8e9191' transform='rotate(90 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.25s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#8e9191' transform='rotate(120 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.3333333333333333s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#8e9191' transform='rotate(150 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.4166666666666667s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#8e9191' transform='rotate(180 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.5s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#8e9191' transform='rotate(210 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.5833333333333334s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#8e9191' transform='rotate(240 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.6666666666666666s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#8e9191' transform='rotate(270 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.75s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#8e9191' transform='rotate(300 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.8333333333333334s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#8e9191' transform='rotate(330 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.9166666666666666s' repeatCount='indefinite'/></rect></svg>
				</div>
			</div>
		</section>
		<header>
			<div class="container">
				<a href="./" class="logo">
					<img src="img/logo.png" alt="Sua Biblioteca">
				</a>
				<!-- float button "Voltar ao Topo" -->
				<a href="#div-topo"><div id="btn-top" class="cd-top"></div></a>
				<nav>
					<ul>
						<li>
							<a href="#como-funciona-anchor">Sobre</a>
						</li>
						<li>
							<a href="#solucoes-azul">Soluções</a>
						</li>
						<li>
							<a href="#pacotes-anchor">Pacotes</a>
						</li>
						<li>
							<a href="#servicos-anchor">Serviços</a>
						</li>
						<li>
							<a href="#contato-anchor">Contato</a>
						</li>
						<li  class="hover-login">
							<a href="#">Login</a>
              <div class="div-login">
                <form class="form-login" action="./dashboard.php">
                  <label for="emailfield">email:</label>
                  <input type="text" name="emailfield">
                  <label for="passwordfield">senha:</label>
                  <input type="password" name="passwordfield">
                  <a class="esqueci-senha" href="#">esqueci minha senha</a>
                  <input type="submit" value="Entrar">
                </form>
              </div>
						</li>
						<li id="login-mobile" class="login-mobile">
              <a href="#" id="title-login-mobile">LOGIN:</a>
							<div>
								<form class="form-login-mobile" action="./dashboard.php">
									<label for="emailfield">e-mail</label>
									<input type="text" class="login-mobile-anchor" name="emailfield">
									<label for="passwordfield">senha</label>
									<input type="password" name="passwordfield">
									<a id="esqueci-senha-mobile" href="#">esqueci minha senha</a>
									<input type="submit" value="Entrar">
								</form>
							</div>
						</li>
					</ul>
				</nav>
			</div>
		</header>
	
		<section id="inicio">
			<div class="container">
				<h1>Organize sua biblioteca na nuvem</h1>
				<h3>Aqui, você conta com softwares de qualidade validados pelo mercado para gerir a sua biblioteca na nuvem com um preço acessível!</h3>

				<a href="#como-funciona-anchor">
					<button class="btn-solucao">
						Começar agora
					</button>
				</a>

				<div class="n-azul n-azul-1"></div>
				<div class="n-azul n-azul-2"></div>
				<div class="n-azul n-azul-3"></div>
				<div class="n-azul n-azul-4"></div>
				<div class="nuvem-branca"></div>
				
				<div class="icones"></div>
			</div>
			<div class="divisao"></div>
		</section>
		<section id="como-funciona">


			<div class="container nuvens-escuras">	
				<div class="escuro-esq"></div>
				<div class="escuro-centro"></div>
				<div class="escuro-dir"></div>
			</div>

			<div id="como-funciona-anchor" class="funciona funciona-1">
				<div class="container">
					<h1>Como Funciona</h1>
					<div class="icon">
						<img class="livro" src="img/livro.png" alt="">
					</div>
					<div class="texto">
						<div class="number">1.</div>
						<p>
							Se você é uma pequena instituição ou possui um acervo pessoal de livros ou qualquer outro tipo de documentação, nós temos a solução para te ajudar a armazenar e organizar toda essa informação.
						</p>
					</div>
					<div class="div-setinha">
					<a href="#funciona2"><img class="img-setinha-solucoes" src="img/setinha.png"></a>
					</div>
				</div>
				<div class="divisao-cinza"></div>
			</div>
			<div id="funciona2" class="funciona funciona-2">
				<div class="container">
					<div class="icon">
						<img class="caixa" src="img/caixa.png" alt="">
					</div>
					<div class="texto">
						<div class="number">2.</div>
						<p>
							Nós temos três tipos de solução: gestão de acervos, indexação inteligente e disponibilização de biblioteca online. Você pode contratar diferentes pacotes de soluções que vão te garantir as soluções que você precisa para a sua biblioteca.
						</p>
					</div>
					<div class="div-setinha">
					<a href="#funciona3"><img class="img-setinha-solucoes" src="img/setinha.png"></a>
					</div>
				</div>
			</div>
			<div id="funciona3" class="funciona funciona-3">
				<div class="container">
					<div class="icon">
						<img src="img/icon-nuvem.png" class="icon-nuvem" alt="">
					</div>
					<div class="texto">
						<div class="number">3.</div>
						<p>
							Depois de escolher o seu pacote, nós configuraremos um servidor na nuvem de maneira que você tenha acesso às soluções contratadas de qualquer lugar, bastando apenas fazer login aqui pelo site!
						</p>
					</div>
					<div class="div-setinha">
					<a href="#solucoes-azul"><img class="img-setinha-solucoes" src="img/setinha.png"></a>
					</div>
				</div>
			</div>
		</section>
		<section id="solucoes-azul">
			<div class="header">
				<div class="container">
					<a href="#" class="logo">
						<img src="img/logo-branca.png" alt="Sua Biblioteca">
					</a>
					<nav>
						<ul>
  						<li>
  							<a href="#como-funciona-anchor">Sobre</a>
  						</li>
  						<li>
  							<a href="#solucoes-azul" class="active">Soluções</a>
  						</li>
  						<li>
  							<a href="#pacotes-anchor">Pacotes</a>
  						</li>
  						<li>
  							<a href="#servicos-anchor">Serviços</a>
  						</li>
  						<li>
  							<a href="#contato-anchor">Contato</a>
  						</li>
  						<li class="hover-login">
  							<a href="#">Login</a>
                <div class="div-login">
                  <form class="form-login" action="./dashboard.php">
                    <label for="emailfield">email:</label>
                    <input type="text" name="emailfield">
                    <label for="passwordfield">senha:</label>
                    <input type="password" name="passwordfield">
                    <a class="esqueci-senha" href="#">esqueci minha senha</a>
                    <input type="submit" value="Entrar">
                  </form>
                </div>
  						</li>
					</nav>
				</div>
			</div>
			<div class="container">
				<h1 class="mobile">Soluções</h1>
				<div class="solucoes">
					<div class="relative">

						<ul>
							<a id="proxima-solucao1" href="#0"><div class="proxima-solucao1"> </div></a>
							<a id="proxima-solucao2" href="#0"><div class="proxima-solucao2"> </div></a>
							<a id="solucao-pratica" href="#0"><div class="solucao-pratica"> </div></a>
							<div class="nuvem">
							</div>
							<div class="linha"></div>
							<li>
								<h3>Gestão de acervo</h3>
								<div class="icone gestao">
									
								</div>
								<p class="mobile">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis laborum repellendus natus nesciunt sint dolores commodi alias voluptate aspernatur, cumque quis facilis numquam? Numquam nulla, blanditiis. Voluptate architecto in velit.
								</p>
							</li>
							<li id="indexacao-inteligente">
								<h3>Indexação Inteligente</h3>
								<div class="icone indexacao">
									
								</div>
								<p class="mobile">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis laborum repellendus natus nesciunt sint dolores commodi alias voluptate aspernatur, cumque quis facilis numquam? Numquam nulla, blanditiis. Voluptate architecto in velit.
								</p>
							</li>
							<li>
								<h3>Biblioteca Online</h3>
								<div class="icone biblioteca">
									
								</div>
								<p class="mobile">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis laborum repellendus natus nesciunt sint dolores commodi alias voluptate aspernatur, cumque quis facilis numquam? Numquam nulla, blanditiis. Voluptate architecto in velit.
								</p>
							</li>
						</ul>

						<div class="text-content">	
							<div class="fade">
								<p class="text gestao">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis, id? Doloremque libero, placeat nihil deserunt deleniti accusantium rem minus recusandae mollitia facilis ab dolorum, eaque error sed molestiae laudantium ipsa. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui vel, dignissimos temporibus mollitia quod numquam earum, ab nesciunt aliquam incidunt repudiandae nulla doloribus eligendi! Cum nam quae enim officia nulla!</p>

								<p class="text indexacao">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis, id? Doloremque libero, placeat nihil deserunt deleniti accusantium rem minus recusandae mollitia facilis ab dolorum, eaque error sed molestiae laudantium ipsa. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui vel, dignissimos temporibus mollitia quod numquam earum, ab nesciunt aliquam incidunt repudiandae nulla doloribus eligendi! Cum nam quae enim officia nulla!</p>

								<p class="text biblioteca">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nobis, id? Doloremque libero, placeat nihil deserunt deleniti accusantium rem minus recusandae mollitia facilis ab dolorum, eaque error sed molestiae laudantium ipsa. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Qui vel, dignissimos temporibus mollitia quod numquam earum, ab nesciunt aliquam incidunt repudiandae nulla doloribus eligendi! Cum nam quae enim officia nulla!</p>
							</div>

						</div>



						<a href="#pacotes-anchor">
							<button class="btn-solucao">Quero essa solução</button>
						</a>


						<div class="seta"></div>
					</div>
				</div>

				<div class="solucoes-mobile">
					<div class="relative">

						<ul>
							<div class="nuvem">
							
							</div>
							<div class="linha"></div>
							<li>
								<h3>Gestão de acervo</h3>
								<div class="icone gestao">
									
								</div>
								<p class="mobile">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis laborum repellendus natus nesciunt sint dolores commodi alias voluptate aspernatur, cumque quis facilis numquam? Numquam nulla, blanditiis. Voluptate architecto in velit.
								</p>
								<a href="#pacotes-anchor">
									<button class="btn-solucao">Quero essa solução</button>
								</a>

							</li>
							<li>
								<h3>Indexação Inteligente</h3>
								<div class="icone indexacao">
									
								</div>
								<p class="mobile">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis laborum repellendus natus nesciunt sint dolores commodi alias voluptate aspernatur, cumque quis facilis numquam? Numquam nulla, blanditiis. Voluptate architecto in velit.
								</p>

								<a href="#pacotes-anchor">
									<button class="btn-solucao">Quero essa solução</button>
								</a>

							</li>
							<li>
								<h3>Biblioteca Online</h3>
								<div class="icone biblioteca">
									
								</div>
								<p class="mobile">
									Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis laborum repellendus natus nesciunt sint dolores commodi alias voluptate aspernatur, cumque quis facilis numquam? Numquam nulla, blanditiis. Voluptate architecto in velit.
								</p>
								<a href="#pacotes-anchor">
									<button class="btn-solucao">Quero essa solução</button>
								</a>

							</li>
						</ul>

	
						<div class="seta"></div>
					</div>
				</div>
			</div>


			
		</section>
		<section id="solucoes-branco">
			<div class="divisao-azul"></div>
			<div class="container">
				<h1>Veja nossas soluções em prática!</h1>

				<div id="video-anchor" class="video">
					<iframe id="iframe-video-home" width="540" height="319" src="https://www.youtube.com/embed/SkmpyzbRsFU" frameborder="0" allowfullscreen></iframe>
				</div>

				<div class="nuvem-esq"></div>
				<div class="nuvem-dir"></div>
				<div class="nuvem-centro"></div>

				<div class="depoimento">
					<div class="img">
						<img src="img/maria.png" alt="">
					</div>
					<div class="texto">
						<p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla placeat nihil accusantium dolore ab ullam repellendus quis provident vero! Perspiciatis laudantium fugiat natus nostrum sapiente error incidunt commodi saepe minus!"</p>
						<span> Maria do Socorro, Coordenadora do setor social do TJGO </span>
					</div>
				</div>
				<div class="div-setinha">
					<img id="setinha2" src="img/setinha.png">
				</div>
			</div>
		</section>
		<section id="parceiros">
			<div class="container">
				<h1>Qualidade Assegurada por nossos fornecedores e parceiros</h1>
				<ul>
					<li>
						<img src="img/parceiros/ibm.png" alt="">
					</li>
					<li>
						<img src="img/parceiros/oracle.png" alt="">
					</li>
					<li>
						<img src="img/parceiros/cisco.png" alt="">
					</li>
					<li>
						<img src="img/parceiros/sun.png" alt="">
					</li>
				</ul>
				<a href="#pacotes-anchor">
					<button class="btn-parceiros">Ver pacotes de soluções</button>
				</a>
			</div>
			<div class="divisao-branca"></div>
		</section>
		<div id="pacotes">
			<div class="container nuvens-escuras">	
				<div class="escuro-esq"></div>
				<div class="escuro-centro"></div>
				<div class="escuro-dir"></div>
			</div>
			<div class="header" >
				<div class="container">
					<a href="#" class="logo">
						<img src="img/logo.png" alt="Sua Biblioteca">
					</a>
					<nav>
						<ul>
  						<li>
  							<a href="#como-funciona-anchor">Sobre</a>
  						</li>
  						<li>
  							<a href="#solucoes-azul">Soluções</a>
  						</li>
  						<li>
  							<a href="#pacotes-anchor" class="active">Pacotes</a>
  						</li>
  						<li>
  							<a href="#servicos-anchor">Serviços</a>
  						</li>
  						<li>
  							<a href="#contato-anchor">Contato</a>
  						</li>
  						<li class="hover-login">
  							<a href="#">Login</a>
                <div class="div-login">
                  <form class="form-login" action="./dashboard.php">
                    <label for="emailfield">email:</label>
                    <input type="text" name="emailfield">
                    <label for="passwordfield">senha:</label>
                    <input type="password" name="passwordfield">
                    <a class="esqueci-senha" href="#">esqueci minha senha</a>
                    <input type="submit" value="Entrar">
                  </form>
                </div>
  						</li>
				
					</nav>
				</div>
			</div>
			<div class="container" id="pacotes-anchor">
				<h1 class="mobile">Pacotes</h1>
				<ul class="planos">
					<li>
						<div class="top">
							<h3>Indexação Inteligente</h3>
							<div class="price">
								<p>
									<span class="moeda">R$</span>
									99
									<span class="mes">/mês</span>
								</p>
							</div>
							<div class="obs">
								<p>
									Perfeito para escritórios de advocacia, pequenas instituições e acervos pessoais.
								</p>
							</div>
						</div>
						<a href="./adicionais.php"><button>Escolher pacote</button></a>
						<div class="bottom">
							<ul>
								<li>Suporte ilimitado</li>
								<li>Indexação inteligente de acervo</li>
								<li>Gestão completa de acervo</li>
								<li>Disponibilização de Biblioteca Online</li>
							</ul>
						</div>
					</li>
					<li>
						<div class="top">
							<h3>Indexação Inteligente</h3>
							<div class="price">
								<p>
									<span class="moeda">R$</span>
									99
									<span class="mes">/mês</span>
								</p>
							</div>
							<div class="obs">
								<p>
									Perfeito para escritórios de advocacia, pequenas instituições e acervos pessoais.
								</p>
							</div>
						</div>
						<a href="./adicionais.php"><button>Escolher pacote</button></a>
						<div class="bottom">
							<ul>
								<li>Suporte ilimitado</li>
								<li>Indexação inteligente de acervo</li>
								<li>Gestão completa de acervo</li>
								<li>Disponibilização de Biblioteca Online</li>
							</ul>
						</div>
					</li>
					<li>
						<div class="top">
							<h3>Indexação Inteligente</h3>
							<div class="price">
								<p>
									<span class="moeda">R$</span>
									99
									<span class="mes">/mês</span>
								</p>
							</div>
							<div class="obs">
								<p>
									Perfeito para escritórios de advocacia, pequenas instituições e acervos pessoais.
								</p>
							</div>
						</div>
						<a href="./adicionais.php"><button>Escolher pacote</button></a>
						<div class="bottom">
							<ul>
								<li>Suporte ilimitado</li>
								<li>Indexação inteligente de acervo</li>
								<li>Gestão completa de acervo</li>
								<li>Disponibilização de Biblioteca Online</li>
							</ul>
						</div>
					</li>
					<li>
						<div class="top">
							<h3>Indexação Inteligente</h3>
							<div class="price">
								<p>
									<span class="moeda">R$</span>
									99
									<span class="mes">/mês</span>
								</p>
							</div>
							<div class="obs">
								<p>
									Perfeito para escritórios de advocacia, pequenas instituições e acervos pessoais.
								</p>
							</div>
						</div>
						<a href="./adicionais.php"><button>Escolher pacote</button></a>
						<div class="bottom">
							<ul>
								<li>Suporte ilimitado</li>
								<li>Indexação inteligente de acervo</li>
								<li>Gestão completa de acervo</li>
								<li>Disponibilização de Biblioteca Online</li>
							</ul>
						</div>
					</li>
				</ul>

				<div class="personalizado">
					<div class="text">
						<h1>Quer um pacote personalizado para suas necessidades?</h1>
						<h2>Podemos criar um pacote específico para você!</h2>
					</div>
					<button class="btn-req-personal">Requisitar pacote</button>
				</div>

			</div>

		</div>
		<div id="duvida">
			<div class="container">
				<h1>Ainda está com dúvida?</h1>
				<h2>Requisite uma demonstração da SuaBiblioteca© por aqui.</h2>
				<button class="btn-req-demo">Requisitar Demonstração</button>
			</div>
		</div>
		<div id="servicos">
			
			<div id="servicos-anchor" class="header">
				<div class="container">
					<a href="#" class="logo">
						<img src="img/logo.png" alt="Sua Biblioteca">
					</a>
					<nav>
						<ul>
  						<li>
  							<a href="#como-funciona-anchor">Sobre</a>
  						</li>
  						<li>
  							<a href="#solucoes-azul">Soluções</a>
  						</li>
  						<li>
  							<a href="#pacotes-anchor">Pacotes</a>
  						</li>
  						<li>
  							<a href="#servicos-anchor" class="active">Serviços</a>
  						</li>
  						<li>
  							<a href="#contato-anchor">Contato</a>
  						</li>
  						<li class="hover-login">
  							<a href="#">Login</a>
                <div class="div-login">
                  <form class="form-login" action="./dashboard.php">
                    <label for="emailfield">email:</label>
                    <input type="text" name="emailfield">
                    <label for="passwordfield">senha:</label>
                    <input type="password" name="passwordfield">
                    <a class="esqueci-senha" href="#">esqueci minha senha</a>
                    <input type="submit" value="Entrar">
                  </form>
                </div>
  						</li>
				
					</nav>
				</div>
			</div>
			<div class="container">
				<h1>Conheça os serviços que temos para a sua biblioteca</h1>

				<ul class="servicos">
					<li>
						<h2>Título do serviço</h2>
						<img src="#" alt="">
						<div class="texto">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem voluptatibus tenetur temporibus esse, dolor, sapiente alias delectus deserunt totam maxime incidunt officiis numquam cumque cum et fugit culpa suscipit quidem!</p>
							<button class="btn-pedido-orcamento">Requisitar Orçamento</button>
						</div>
					</li>
					<li>
						<h2>Título do serviço</h2>
						<img src="#" alt="">
						<div class="texto">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem voluptatibus tenetur temporibus esse, dolor, sapiente alias delectus deserunt totam maxime incidunt officiis numquam cumque cum et fugit culpa suscipit quidem!</p>
							<button class="btn-pedido-orcamento">Requisitar Orçamento</button>
						</div>
					</li>
					<li>
						<h2>Título do serviço</h2>
						<img src="#" alt="">
						<div class="texto">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem voluptatibus tenetur temporibus esse, dolor, sapiente alias delectus deserunt totam maxime incidunt officiis numquam cumque cum et fugit culpa suscipit quidem!</p>
							<button class="btn-pedido-orcamento">Requisitar Orçamento</button>
						</div>
					</li>
					<li>
						<h2>Título do serviço</h2>
						<img src="#" alt="">
						<div class="texto">
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem voluptatibus tenetur temporibus esse, dolor, sapiente alias delectus deserunt totam maxime incidunt officiis numquam cumque cum et fugit culpa suscipit quidem!</p>
							<button class="btn-pedido-orcamento">Requisitar Orçamento</button>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div id="contato">
			<div id="contato-anchor" class="header">
				<div class="container">
					<a href="#" class="logo">
						<img src="img/logo-branca.png" alt="Sua Biblioteca">
					</a>
					<nav>
						<ul>
  						<li>
  							<a href="#como-funciona-anchor">Sobre</a>
  						</li>
  						<li>
  							<a href="#solucoes-azul">Soluções</a>
  						</li>
  						<li>
  							<a href="#pacotes-anchor">Pacotes</a>
  						</li>
  						<li>
  							<a href="#servicos-anchor">Serviços</a>
  						</li>
  						<li>
  							<a href="#contato-anchor" class="active">Contato</a>
  						</li>
  						<li class="hover-login">
  							<a href="#">Login</a>
                <div class="div-login">
                  <form class="form-login" action="./dashboard.php">
                    <label for="emailfield">email:</label>
                    <input type="text" name="emailfield">
                    <label for="passwordfield">senha:</label>
                    <input type="password" name="passwordfield">
                    <a class="esqueci-senha" href="#">esqueci minha senha</a>
                    <input type="submit" value="Entrar">
                  </form>
                </div>
  						</li>
				
					</nav>
				</div>
			</div>
			<div class="container">
				<h1>Tem alguma dúvida? Entre em contato!</h1>
				<h2>Te responderemos em, até, 24h</h2>

				<form class="formcontato" action="#">
					<label class="labelform" for="name">nome:</label>
					<input class="inputform" type="text" name="name" id="name">
					<label class="labelform" for="name">e-mail:</label>
					<input class="inputform" type="text" name="email" id="email">
					<label class="labelform" for="message">mensagem:</label>
					<textarea name="message" id="message"></textarea>
					<button class="buttonform" type="submit">Enviar</button>
				</form>
			</div>
		</div>

	<?php include 'includes/footer.php' ?>