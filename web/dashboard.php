<?php include 'includes/head.php' ?>
	<header class="account">
		<div class="container">
				<a href="./" class="logo">
					<img src="img/logo-branca.png" alt="Sua Biblioteca">
				</a>
				<nav>
					<div class="bemvindo">
						<span>Olá,</span> Maria do Socorro
					</div>
					<ul>
						<li>
							<a href="#" class="active">Dashboard</a>
						</li>
						<li>
							<a href="./minha-conta.php">Minha Conta</a>
						</li>
						<li>
							<a href="./suporte.php">Suporte</a>
						</li>
						<li>
							<a href="./">Sair</a>
						</li>
					</ul>
				</nav>
			</div>
			<div class="divisao-azul"></div>
	</header>
	<section id="dashboard" class="nuvens-bg">
		<div class="container">
			<div class="seta">
				<div class="h-l">
					<h1>Link para sua Biblioteca:</h1>
					<a href="#">http://www.suabiblioteca.com.br</a>
				</div>
				<div class="h-r">
					<div class="pacote">
						<h3>SEU PACOTE</h3>
						<h2>NOME DO PACOTE</h2>
						<a href="./#pacotes-anchor"> > consultar outros pacotes</a>
					</div>
					<div class="pagamento">
						<h3>STATUS DO PAGAMENTO</h3>
						<h2>OK</h2>
						<div class="venc"> > vencimento: 20/02/2015</div>
					</div>
				</div>
			</div>
			<div class="seta">
				<div class="h-l">
					<h1>Gere seu boleto de pagamento</h1>
					<p>Você pode gerar os seus boletos por período. Quanto maior for o período do boleto que você gerar, maior será o desconto.</p>
				</div>
				<div class="h-r">
					<div class="h-left">
						<span>Do mês:</span>
						<select name="" id="">
							<option value="">Outubro / 2015</option>
						</select>

						<span class="desconto">de R$250,00</span>
						<div class="price">
							<span>por </span>R$ 199,00
						</div>
					</div>
					<div class="h-right">
						<span>Ao mês:</span>
						<select name="" id="">
							<option value="">Outubro / 2015</option>
						</select>
						<button>
							Gerar boleto
						</button>
					</div>
				</div>
			</div>
			<div class="seta">
				<div class="h-l" id="seta-pagamento-cartao">
					<h1>Habilite pagamento via Cartão</h1>
					<p>Insira os dados do seu cartão de crédito e realize o pagamento da sua fatura de forma automática todo mês.</p>
				</div>
				<form>
					<div class="h-r">
						<div class="h-left cartao-dashboard-mobile">
							<span><label for="nometitular">nome do titular:</label></span>
							<input type="text" name="nometitular">
						
							<div class="div-codigoseguranca">
								<span><label for="codigoseguranca">cód. de segurança:</label></span>
								<input type="text" name="codigoseguranca" id="campo-codseg-dashboard">
							</div>
							
							<div class="div-validade">
								<span><label for="validade">validade:</label></span>
								<input type="text" class="validade-cartao-mes" name="validademes" placeholder="mm">
								<input type="text" class="validade-cartao-ano" name="validadeano" placeholder="aaaa">
							</div>
						
						</div>
						<div class="h-right cartao-dashboard-mobile"> 
							<span><label for="numerocartao">número do Cartão:</label></span>
							<input type="text" name="numerocartao">
							<button>
								Habilitar
							</button>
						</div>
					</div>
				</form>
			</div>
			<div class="seta">
				<div class="h-l" id="seta-modo-pagamento">
					<h1>Seus dados de pagamento</h1>
					<p>Para atualizar os seus dados de pagamento, basta alterar as informações e clicar no botão "atualizar".</p>
					<a id="voltar-boleto">> Quero voltar a pagar via boleto</a>
				</div>
				<form>
					<div class="h-r">
						<div class="h-left cartao-dashboard-mobile">
							<span><label for="nometitular">nome do titular:</label></span>
							<input type="text" name="nometitular" placeholder="Maria do Socorro Bizantino Freitas">
						
							<div class="div-codigoseguranca">
								<span><label for="codigoseguranca">cód. seg.:</label></span>
								<input type="text" name="codigoseguranca" id="campo-codseg-dashboard" placeholder="127">
							</div>
							
							<div class="div-validade">
								<span><label for="validade">validade:</label></span>
								<input type="text" class="validade-cartao-mes" name="validademes" placeholder="10">
								<input type="text" class="validade-cartao-ano" name="validadeano" placeholder="2021">
							</div>
						</div>
						<div class="h-right cartao-dashboard-mobile">
							<span><label for="numerocartao">número do Cartão:</label></span>
							<input type="text" name="numerocartao" placeholder="*****8907">
							<button>
								Atualizar
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</section>
<?php include 'includes/footer.php' ?>
