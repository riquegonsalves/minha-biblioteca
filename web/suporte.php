<?php include 'includes/head.php' ?>
	<header class="account">
		<div class="container">
				<a href="./" class="logo">
					<img src="img/logo-branca.png" alt="Sua Biblioteca">
				</a>
				<nav>
					<div class="bemvindo">
						<span>Olá,</span> Maria do Socorro
					</div>
					<ul>
						<li>
							<a href="./dashboard.php">Dashboard</a>
						</li>
						<li>
							<a href="./minha-conta.php">Minha Conta</a>
						</li>
						<li>
							<a href="#" class="active">Suporte</a>
						</li>
						<li>
							<a href="./">Sair</a>
						</li>
					</ul>
				</nav>
			</div>
			<div class="divisao-azul"></div>
	</header>
	<section id="suporte" class="nuvens-bg">
		<div class="container">
			<div class="all">
				<button class="new"><span>+</span> Novo Chamado</button>
				<div class="box">
					<div class="title">
						Meus Chamados
					</div>
					<div class="content">
						<ul>
							<li>
								<div class="top">
									<h2>Titulo do Chamado</h2>
									<div class="hr">8h</div>
								</div>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias dolor delectus mamakksnd ak...</p>
								<div class="sticker resolvido">Resolvido</div>
								<div class="sticker pendente">Pendente</div>
							</li>
							<li>
								<div class="top">
									<h2>Titulo do Chamado</h2>
									<div class="hr">8h</div>
								</div>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias dolor delectus mamakksnd ak...</p>
								<div class="sticker resolvido">Resolvido</div>
								<div class="sticker pendente">Pendente</div>
							</li>
							<li>
								<div class="top">
									<h2>Titulo do Chamado</h2>
									<div class="hr">8h</div>
								</div>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias dolor delectus mamakksnd ak...</p>
								<div class="sticker resolvido">Resolvido</div>
								<div class="sticker pendente">Pendente</div>
							</li>
							<li>
								<div class="top">
									<h2>Titulo do Chamado</h2>
									<div class="hr">8h</div>
								</div>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias dolor delectus mamakksnd ak...</p>
								<div class="sticker resolvido">Resolvido</div>
								<div class="sticker pendente">Pendente</div>
							</li>
							
						</ul>
						<a href="#" class="ver-mais">Ver mais</a>
					</div>
				</div>
			</div>
			<div class="single">
				<div class="box">
					
					<div class="title">
						<h2>[ Último chamado ] "Chama na Brasa"</h2>
					</div>
					<div class="content">
						<div class="talk">
							<ul>
								<li class="enviado">
									<div class="time">
										8h
									</div>
									<div class="text">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde recusandae voluptates repellat consequuntur deserunt. Optio pariatur laboriosam voluptas ex, placeat delectus molestias voluptatem esse repudiandae culpa commodi sed, numquam quibusdam?
									</div>
								</li>
								<li class="recebido">
									<div class="time">
										8h
									</div>
									<div class="text">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde recusandae voluptates repellat consequuntur deserunt. Optio pariatur laboriosam voluptas ex, placeat delectus molestias voluptatem esse repudiandae culpa commodi sed, numquam quibusdam?
									</div>
								</li>
								<li class="enviado">
									<div class="time">
										8h
									</div>
									<div class="text">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde recusandae voluptates repellat consequuntur deserunt. Optio pariatur laboriosam voluptas ex, placeat delectus molestias voluptatem esse repudiandae culpa commodi sed, numquam quibusdam?
									</div>
								</li>
								<li class="recebido">
									<div class="time">
										8h
									</div>
									<div class="text">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde recusandae voluptates repellat consequuntur deserunt. Optio pariatur laboriosam voluptas ex, placeat delectus molestias voluptatem esse repudiandae culpa commodi sed, numquam quibusdam?
									</div>
								</li>
								<li class="enviado">
									<div class="time">
										8h
									</div>
									<div class="text">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde recusandae voluptates repellat consequuntur deserunt. Optio pariatur laboriosam voluptas ex, placeat delectus molestias voluptatem esse repudiandae culpa commodi sed, numquam quibusdam?
									</div>
								</li>
								<li class="recebido">
									<div class="time">
										8h
									</div>
									<div class="text">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde recusandae voluptates repellat consequuntur deserunt. Optio pariatur laboriosam voluptas ex, placeat delectus molestias voluptatem esse repudiandae culpa commodi sed, numquam quibusdam?
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="area">
						<textarea name="" id=""></textarea>
						<button type="submit">Enviar</button>
					</div>
				</div>
			</div>
			
		</div>
	</section>
	<section id="novo-chamado">
		<div class="container">
			<div class="close"> x </div>
			<div class="nuvem-top-esq"></div>
			<div class="nuvem-top-dir"></div>
			<div class="nuvem-bottom-esq"></div>
			<div class="nuvem-bottom-dir"></div>
			<h1>Novo chamado</h1>
			<form action="#">		
				<div class="center">
					<label for="subject">Assunto:</label>
					<input type="text" name="subject">
					<label for="need">Mensagem:</label>
					<textarea name="message" id="message" ></textarea>
				</div>
				<button>Enviar chamado</button>
			</form>
		</div>
	</section>
<?php include 'includes/footer.php' ?>