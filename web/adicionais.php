<?php include "includes/head.php" ?>
<header class="account">
	<div class="container">
		<a href="./" class="logo">
			<img src="img/logo-branca.png" alt="Sua Biblioteca">
		</a>
		<nav>
			<ul>
				<li>
					<a href="/#como-funciona-anchor">Sobre</a>
				</li>
				<li>
					<a href="/#solucoes-azul">Soluções</a>
				</li>
				<li>
					<a href="/#pacotes-anchor">Pacotes</a>
				</li>
				<li>
					<a href="/#servicos-anchor">Serviços</a>
				</li>	
				<li>
					<a href="/#contato-anchor">Contato</a>
				</li>
				<li class="hover-login">
					<a href="#">Login</a>
					<div class="div-login">
						<form class="form-login" action="./dashboard.php">
							<label for="emailfield">email:</label>
							<input type="text" name="emailfield">
							<label for="passwordfield">senha:</label>
							<input type="password" name="passwordfield">
							<a class="esqueci-senha" href="#">esqueci minha senha</a>
							<input type="submit" value="Entrar">
						</form>
					</div>
				</li>
			</ul>
		</nav>
	</div>
	<div class="divisao-azul"></div>
</header>
<section class="nuvens-bg" id="compras-casadas">
	<div class="container">
		<div class="passo-passo">
			<div class="passo active">
				<p>ESCOLHA O PACOTE</p>
				<div class="num"> 1 </div>
			</div>
			<div class="passo active">
				<p>ESCOLHA OS SERVIÇOS</p>
				<div class="num"> 2 </div>
			</div>
			<div class="passo">
				<p>FAÇA O PAGAMENTO</p>
				<div class="num"> 3 </div>
			</div>
		</div>
		<h1>Quer otimizar o seu pacote? Veja nossos serviços!</h1>
		<a href="./compra.php" class="prosseguir"> Prosseguir para pagamento > </a>
	</div>
	<div class="container">
		<ul class="servicos">
			<li>
				<h1>Nome do serviço</h1>
				<img src="img/servico.png" alt="">
				<ul>
					<li>Descrição breve sobre o serviço
Descrição breve sobre o serviço
Descrição breve sobre o serviço
Descrição breve sobre o serviço
Descrição breve sobre o serviço</li>
					
				</ul>
				<div class="check">
					<div class="orcamento">
						Requisitar Orçamento
					</div>
				</div>

			</li>
			<li>
				<h1>Nome do serviço</h1>
				<img src="img/servico.png" alt="">
				<ul>
					<li>Descrição breve sobre o serviço
Descrição breve sobre o serviço
Descrição breve sobre o serviço
Descrição breve sobre o serviço
Descrição breve sobre o serviço</li>
					
				</ul>
				<div class="check">
					<div class="orcamento checked">
						Requisitar Orçamento
					</div>
				</div>

			</li>
			<li>
				<h1>Nome do serviço</h1>
				<img src="img/servico.png" alt="">
				<ul>
					<li>Descrição breve sobre o serviço
Descrição breve sobre o serviço
Descrição breve sobre o serviço
Descrição breve sobre o serviço
Descrição breve sobre o serviço</li>
					
				</ul>
				<div class="check">
					<div class="orcamento">
						Requisitar Orçamento
					</div>
				</div>

			</li>
			<li>
				<h1>Nome do serviço</h1>
				<img src="img/servico.png" alt="">
				<ul>
					<li>Descrição breve sobre o serviço
Descrição breve sobre o serviço
Descrição breve sobre o serviço
Descrição breve sobre o serviço
Descrição breve sobre o serviço</li>
					
				</ul>
				<div class="check">
					<div class="orcamento">
						Requisitar Orçamento
					</div>
				</div>

			</li>
			<li>
				<h1>Nome do serviço</h1>
				<img src="img/servico.png" alt="">
				<ul>
					<li>Descrição breve sobre o serviço
Descrição breve sobre o serviço
Descrição breve sobre o serviço
Descrição breve sobre o serviço
Descrição breve sobre o serviço</li>
					
				</ul>
				<div class="check">
					<div class="orcamento">
						Requisitar Orçamento
					</div>
				</div>

			</li>
			<li>
				<h1>Nome do serviço</h1>
				<img src="img/servico.png" alt="">
				<ul>
					<li>Descrição breve sobre o serviço
Descrição breve sobre o serviço
Descrição breve sobre o serviço
Descrição breve sobre o serviço
Descrição breve sobre o serviço</li>
					
				</ul>
				<div class="check">
					<div class="orcamento">
						Requisitar Orçamento
					</div>
				</div>

			</li>
			
					
		</ul>
	</div>
	<div class="container">
		<a href="./compra.php" class="prosseguir"> Prosseguir para pagamento > </a>
	</div>
</section>
<?php include "includes/footer.php" ?>